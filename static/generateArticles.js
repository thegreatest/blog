const fs = require("fs");

const main = () => {
  let data = fs.readFileSync(__dirname + "/../content/articles/great-article.md");

  for (let i = 3000; i < 7001; ++i) {
    fs.writeFileSync(`${__dirname}/../content/articles/great-article-${i}.md`, data);
  }

};

main();
